﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
	public class ModeloDatos
	{
		public int Id { get; set; }
		public string Sucursal { get; set; }
		public string Categoria { get; set; }
		public string Marca { get; set; }
		public string Producto { get; set; }
		public decimal? Precio { get; set; }
		public string UrlImagen { get; set; }
	}
}