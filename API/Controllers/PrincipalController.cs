﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace API.Controllers
{
    public class PrincipalController : ApiController
    {
	    private readonly baseDataContext _context;
	    public PrincipalController()
	    {
		    _context = new baseDataContext();
	    }
	    public IEnumerable<Models.ModeloDatos> Get()
	    {
			List<Models.ModeloDatos> result = new List<Models.ModeloDatos>();
		    var query = from produc in _context.productos
			    join sucur in _context.sucursales on produc.sucursales_id equals sucur.sucursal_id
			    join catego in _context.categorias on produc.categoria_id equals catego.categoria_id
			    join marc in _context.marca on produc.marca_id equals marc.marca_id
			    select new
			    {
				    proid = produc.id,sucurs = sucur.sucursales1,categ = catego.categoria,
					mar = marc.marca1, pro = produc.producto,prec = produc.precio,url = produc.URL_IMG
			    };
		    var lista = query.ToList();
		    foreach (var item in lista)
		    {
			    result.Add(new Models.ModeloDatos()
			    {
				    Id = item.proid,Sucursal = item.sucurs, Categoria = item.categ,
					Marca = item.mar, Producto = item.pro, Precio = item.prec,UrlImagen = item.url
			    });
		    }
		    return result.AsEnumerable();
		}
		[HttpGet]
	    [Route("api/principal/sucursales")]
	    public IEnumerable<sucursales> sucursal()
	    {
			List<sucursales> result = new List<sucursales>();
		    var query = from sucurs in _context.sucursales 
			    select new
			    {
				    id = sucurs.sucursal_id,
					sucursal = sucurs.sucursales1
			    };
		    var lista = query.ToList();
		    foreach (var item in lista)
		    {
			    result.Add(new sucursales()
			    {
				    sucursal_id = item.id,
					sucursales1 = item.sucursal
			    });
		    }
		    return result.AsEnumerable();
		}
		[HttpGet]
	    [Route("api/principal/categoria")]
	    public IEnumerable<categorias> categ()
	    {
		    List<categorias> result = new List<categorias>();
		    var query = from categ in _context.categorias
			    select new
			    {
				    id = categ.categoria_id,
				    categ = categ.categoria
			    };
		    var lista = query.ToList();
		    foreach (var item in lista)
		    {
			    result.Add(new categorias()
			    {
					categoria_id = item.id,
					categoria = item.categ
			    });
		    }
		    return result.AsEnumerable();
	    }
		[HttpGet]
	    [Route("api/principal/marca")]
	    public IEnumerable<marca> marca()
	    {
		    List<marca> result = new List<marca>();
		    var query = from marc in _context.marca
			    select new
			    {
				    id = marc.marca_id,
				    marca = marc.marca1
			    };
		    var lista = query.ToList();
		    foreach (var item in lista)
		    {
			    result.Add(new marca()
			    {
				    marca_id = item.id,
				    marca1 = item.marca
			    });
		    }
		    return result.AsEnumerable();
	    }
	}
}
